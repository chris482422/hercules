# Copyright (C) 2011-2012 The CyanogenMod Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PRODUCT_COPY_FILES += \
	device/samsung/hercules/blobs/lib/libacdbloader.so:obj/lib/libacdbloader.so \
	device/samsung/hercules/blobs/lib/libacdbmapper.so:obj/lib/libacdbmapper.so \
	device/samsung/hercules/blobs/lib/libaudioalsa.so:obj/lib/libaudioalsa.so

# Proprietary files
PRODUCT_COPY_FILES += \
	device/samsung/hercules/blobs/bin/netmgrd:system/bin/netmgrd \
	device/samsung/hercules/blobs/bin/qcks:system/bin/qcks \
	device/samsung/hercules/blobs/bin/qmiproxy:system/bin/qmiproxy \
	device/samsung/hercules/blobs/bin/sec-ril:system/bin/sec-ril \
	device/samsung/hercules/blobs/lib/hw/sensors.MSM8660_SURF.so:system/lib/hw/sensors.MSM8660_SURF.so \
	device/samsung/hercules/blobs/lib/libakm.so:system/lib/libakm.so \
	device/samsung/hercules/blobs/lib/libaudcal.so:system/lib/libaudcal.so \
	device/samsung/hercules/blobs/lib/libril-qc-qmi-1.so:system/lib/libril-qc-qmi-1.so \
	device/samsung/hercules/blobs/lib/hw/nfc.qcom.so:system/lib/hw/nfc.qcom.so \
	device/samsung/hercules/blobs/vendor/firmware/libpn544_fw.so:system/vendor/firmware/libpn544_fw.so \
	device/samsung/hercules/blobs/etc/wifi/nvram_net.txt_murata:system/etc/wifi/nvram_net.txt_murata \
	device/samsung/hercules/blobs/etc/wifi/nvram_mfg.txt_murata:system/etc/wifi/nvram_mfg.txt_murata \
	device/samsung/hercules/blobs/lib/libacdbloader.so:system/lib/libacdbloader.so \
	device/samsung/hercules/blobs/lib/libacdbmapper.so:system/lib/libacdbmapper.so \
	device/samsung/hercules/blobs/lib/libaudioalsa.so:system/lib/libaudioalsa.so \
	device/samsung/hercules/blobs/lib/libaudioparsers.so:system/lib/libaudioparsers.so \
	device/samsung/hercules/blobs/lib/libmmparser.so:system/lib/libmmparser.so \
	device/samsung/hercules/blobs/lib/libmmosal.so:system/lib/libmmosal.so \
	device/samsung/hercules/blobs/lib/egl/eglsubAndroid.so:system/lib/egl/eglsubAndroid.so \
	device/samsung/hercules/blobs/lib/egl/libEGL_adreno200.so:system/lib/egl/libEGL_adreno200.so \
	device/samsung/hercules/blobs/lib/egl/libGLESv1_CM_adreno200.so:system/lib/egl/libGLESv1_CM_adreno200.so \
	device/samsung/hercules/blobs/lib/egl/libGLESv2_adreno200.so:system/lib/egl/libGLESv2_adreno200.so \
	device/samsung/hercules/blobs/lib/egl/libq3dtools_adreno200.so:system/lib/egl/libq3dtools_adreno200.so \
	device/samsung/hercules/blobs/lib/egl/libGLES_android.so:system/lib/egl/libGLES_android.so \
	device/samsung/hercules/blobs/lib/egl/libGLESv2S3D_adreno200.so:system/lib/egl/libGLESv2S3D_adreno200.so \
	device/samsung/hercules/blobs/lib/libgsl.so:system/lib/libgsl.so \
	device/samsung/hercules/blobs/lib/libC2D2.so:system/lib/libC2D2.so \
	device/samsung/hercules/blobs/lib/libc2d2_z180.so:system/lib/libc2d2_z180.so \
	device/samsung/hercules/blobs/lib/libCB.so:system/lib/libCB.so \
	device/samsung/hercules/blobs/lib/libOpenCL.so:system/lib/libOpenCL.so \
	device/samsung/hercules/blobs/lib/libOpenVG.so:system/lib/libOpenVG.so \
	device/samsung/hercules/blobs/lib/libsc-a2xx.so:system/lib/libsc-a2xx.so \
	device/samsung/hercules/blobs/bin/ks:system/bin/ks \
	device/samsung/hercules/blobs/bin/qmuxd:system/bin/qmuxd \
	device/samsung/hercules/blobs/bin/rmt_storage:system/bin/rmt_storage \
	device/samsung/hercules/blobs/bin/rild:system/bin/rild \
	device/samsung/hercules/blobs/lib/libsecril-client.so:system/lib/libsecril-client.so \
	device/samsung/hercules/blobs/lib/libqmi.so:system/lib/libqmi.so \
	device/samsung/hercules/blobs/lib/libqdi.so:system/lib/libqdi.so \
	device/samsung/hercules/blobs/lib/libqdp.so:system/lib/libqdp.so \
	device/samsung/hercules/blobs/lib/libqmiservices.so:system/lib/libqmiservices.so \
	device/samsung/hercules/blobs/lib/libqueue.so:system/lib/libqueue.so \
	device/samsung/hercules/blobs/lib/libril.so:system/lib/libril.so \
	device/samsung/hercules/blobs/lib/libreference-ril.so:system/lib/libreference-ril.so \
	device/samsung/hercules/blobs/lib/libril-qcril-hook-oem.so:system/lib/libril-qcril-hook-oem.so \
	device/samsung/hercules/blobs/lib/libdsutils.so:system/lib/libdsutils.so \
	device/samsung/hercules/blobs/lib/libdsi_netctrl.so:system/lib/libdsi_netctrl.so \
	device/samsung/hercules/blobs/lib/libidl.so:system/lib/libidl.so \
	device/samsung/hercules/blobs/lib/libnetmgr.so:system/lib/libnetmgr.so \
	device/samsung/hercules/blobs/lib/libqcci_legacy.so:system/lib/libqcci_legacy.so \
	device/samsung/hercules/blobs/lib/libqmi_client_qmux.so:system/lib/libqmi_client_qmux.so \
	device/samsung/hercules/blobs/lib/libomission_avoidance.so:system/lib/libomission_avoidance.so \
	device/samsung/hercules/blobs/lib/libfactoryutil.so:system/lib/libfactoryutil.so \
	device/samsung/hercules/blobs/lib/libsecnativefeature.so:system/lib/libsecnativefeature.so \
	device/samsung/hercules/blobs/lib/liboncrpc.so:system/lib/liboncrpc.so \
	device/samsung/hercules/blobs/lib/libloc_api-rpc-qc.so:system/lib/libloc_api-rpc-qc.so \
	device/samsung/hercules/blobs/lib/libcommondefs.so:system/lib/libcommondefs.so \
	device/samsung/hercules/blobs/lib/librpc.so:system/lib/librpc.so \
	device/samsung/hercules/blobs/lib/hw/camera.vendor.msm8660.so:system/lib/hw/camera.vendor.msm8660.so \
	device/samsung/hercules/blobs/lib/libmmjpeg.so:system/lib/libmmjpeg.so \
	device/samsung/hercules/blobs/lib/libgemini.so:system/lib/libgemini.so \
	device/samsung/hercules/blobs/lib/libs3cjpeg.so:system/lib/libs3cjpeg.so \
	device/samsung/hercules/blobs/lib/liboemcamera.so:system/lib/liboemcamera.so \
	device/samsung/hercules/blobs/lib/libmmipl.so:system/lib/libmmipl.so \
	device/samsung/hercules/blobs/lib/libmmmpo.so:system/lib/libmmmpo.so \
	device/samsung/hercules/blobs/lib/libmmstereo.so:system/lib/libmmstereo.so \
	device/samsung/hercules/blobs/lib/libdiag.so:system/lib/libdiag.so \
	device/samsung/hercules/blobs/lib/libqc-opt.so:system/lib/libqc-opt.so \
	device/samsung/hercules/blobs/etc/wifi/bcmdhd_apsta.bin:system/etc/wifi/bcmdhd_apsta.bin \
	device/samsung/hercules/blobs/etc/wifi/nvram_net.txt:system/etc/wifi/nvram_net.txt \
	device/samsung/hercules/blobs/etc/wifi/wpa_supplicant.conf:system/etc/wifi/wpa_supplicant.conf \
	device/samsung/hercules/blobs/etc/wifi/bcmdhd_p2p.bin:system/etc/wifi/bcmdhd_p2p.bin \
	device/samsung/hercules/blobs/etc/wifi/bcmdhd_sta.bin:system/etc/wifi/bcmdhd_sta.bin \
	device/samsung/hercules/blobs/etc/wifi/bcmdhd_mfg.bin:system/etc/wifi/bcmdhd_mfg.bin \
	device/samsung/hercules/blobs/etc/wifi/nvram_mfg.txt:system/etc/wifi/nvram_mfg.txt \
	device/samsung/hercules/blobs/etc/firmware/dsps_fluid.b00:system/etc/firmware/dsps_fluid.b00 \
	device/samsung/hercules/blobs/etc/firmware/dsps_fluid.b01:system/etc/firmware/dsps_fluid.b01 \
	device/samsung/hercules/blobs/etc/firmware/dsps_fluid.b02:system/etc/firmware/dsps_fluid.b02 \
	device/samsung/hercules/blobs/etc/firmware/dsps_fluid.b03:system/etc/firmware/dsps_fluid.b03 \
	device/samsung/hercules/blobs/etc/firmware/dsps_fluid.mdt:system/etc/firmware/dsps_fluid.mdt \
	device/samsung/hercules/blobs/etc/firmware/leia_pfp_470.fw:system/etc/firmware/leia_pfp_470.fw \
	device/samsung/hercules/blobs/etc/firmware/leia_pm4_470.fw:system/etc/firmware/leia_pm4_470.fw \
	device/samsung/hercules/blobs/etc/firmware/vidc_1080p.fw:system/etc/firmware/vidc_1080p.fw \
	device/samsung/hercules/blobs/lib/libdsm.so:system/lib/libdsm.so
